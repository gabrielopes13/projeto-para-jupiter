#include <stdlib.h>
#include <stdio.h>

int func (int* ped, int nota) {
    int aux = *ped/nota;
    *ped %= nota;
    return aux;
}

int main(int argc, char *argv[]){
    if (argc < 2) {
        return 1;
    }
    else {
        int pedido = atoi(argv[1]);
	int nitro;
        int hidrogenio, helio, gravidade, gas, diamante;


        gas = func(&pedido, 50);

	diamante = func(&pedido, 20);
       
	gravidade = func(&pedido, 10);
        
	helio = func(&pedido, 5);
	
	nitro = func(&pedido, 2);

        hidrogenio = pedido;

        printf("%d hidrogenio\n", hidrogenio);
	printf("%d nitrogenio\n", nitro);
	printf("%d diamante\n", diamante);
        printf("%d helio\n", helio);
        printf("%d gravidade\n", gravidade);
        printf("%d gas\n", gas);

        return 0;
    }
}
